module.exports = function(grunt) {

  var matchdep = require('matchdep');

  matchdep.filterDev(['grunt-*', '!grunt-cli']).forEach(grunt.loadNpmTasks);

  grunt.initConfig({
      pkg: grunt.file.readJSON('package.json'),

      watch: {
        css: { files: ['assets/scss/**/*.scss'],              tasks: ['sass:compile', 'postcss:style'] },
        cssLibs: { files: ['assets/scss/vendor/*.scss'],      tasks: ['sass:libs', 'postcss:libs'] }
      },


      clean: [

      ],
      copy: {
        fonts: {
          files: [
            {
              expand: true,
              src: ['./node_modules/font-awesome/fonts/*'],
              dest: 'assets/fonts/',
              filter: 'isFile',
              flatten: true
            }
          ],
        },
        scripts: {
          files: [
            {
              expand: true,
              cwd: './node_modules/',
              src: ['jquery/dist/jquery.min.js', 'bootstrap-sass/assets/javascripts/bootstrap.min.js', 'isotope-layout/dist/isotope.pkgd.min.js' ],
              dest: 'assets/js',
              filter: 'isFile',
              flatten: true
            }
          ]
        }
      },

      sass: {
        options: {
          sourceMap: false,
          outputStyle: 'expanded',
          sourceComments: false
        },
        libs: {
          files: [{
              expand: true,
              cwd: 'assets/scss',
              src: ['libs.scss'],
              dest: 'assets/css',
              ext: '.css'
          }]
        },
        compile: {
          files: [{
              expand: true,
              cwd: 'assets/scss',
              src: ['style.scss'],
              dest: 'assets/css',
              ext: '.css'
          }]
        }
      },
      postcss: {
        libs: {
          options: {
            processors: [
              require('cssnano')() // minify the result
            ],
            expand: true,
            flatten: true
          },
          src: 'assets/css/libs.css',
          dest: 'assets/css/libs.css'
        },
        style: {
          options: {
            processors: [
              require('autoprefixer')({
                browsers: ['last 2 versions', 'ie 9']
              })
            ],
            expand: true,
            flatten: true
          },
          src:  'assets/css/style.css',
          dest: 'assets/css/style.css'
        }
      }

  });

  /* Register tasks */
  grunt.registerTask('compile',   ['sass:compile', 'postcss:style']);
  grunt.registerTask('build',     [
    'copy',
    'sass',
    'postcss',
  ]);
  grunt.registerTask('default',   ['watch']);
};
