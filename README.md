# README #

This is the front-end layer of blog template.

### What is this repository for? ###

* Static template for implementation into Django CMS
* Version: 1.0.0


### How do I get set up? ###

* Clone repo
* Install all npm dependencies
```
#!bash

npm install
```

* Run Grunt first iteration
```
#!bash

grunt build --save-dev
```
* Ready to go.


### Knowed issues ###

1. Working on developer server with ARM architecture (odroid, raspbery pi etc.), there can appear an 404 error while installing npm dependencies. Strictly speaking in sass-node module. If so, wait until npm compile everything, then run:

```
#!bash

npm rebuild node-sass
```

Explanation:
>Supplying prebuilt binaries is predicated on a representative of that community taking the responsibility of generating the binaries. Currently binary generation is either automated or handled by a contributor familiar with a specific platform.
>
>However the node-sass install should fallback to generating the binary locally once the download attempt fails. If this is not happening that is a bug.

[Source](https://github.com/sass/node-sass/issues/1340)



### Who do I talk to? ###

* https://bitbucket.org/Harrune/