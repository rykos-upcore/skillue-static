/*
Main scripts
*/


;( function ( jQuery, $ ) {

  $('#grid').isotope({
    itemSelector: '.post-item', // use a separate class for itemSelector, other than .col-
    percentPosition: true,
    masonry: {
      columnWidth: '.grid-sizer'
    }
  });

})( jQuery, $ );
